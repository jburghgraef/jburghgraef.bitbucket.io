var searchData=
[
  ['old_5fposition_529',['old_position',['../classEncoder__Driver__Lab6__ME305_1_1EncoderDriver.html#ad822fc03fc245a82685753cadb0c3e07',1,'Encoder_Driver_Lab6_ME305.EncoderDriver.old_position()'],['../classEncoder__Driver__ME305_1_1encoderdriver.html#a9197a67456353e0dbcd5f70eb6050e76',1,'Encoder_Driver_ME305.encoderdriver.old_position()'],['../classEncoderDriver_1_1EncoderDriver.html#a75ad6c97dc2c785375a256a0a1b31e74',1,'EncoderDriver.EncoderDriver.old_position()']]],
  ['omega_530',['omega',['../UI__FrontEnd__Lab6__ME305_8py.html#ab5d84d06ac067a0422c0daa4481c5015',1,'UI_FrontEnd_Lab6_ME305.omega()'],['../UI__FrontEnd__Lab7__ME305_8py.html#a8e6d9571432c9d4a27613e8856f33870',1,'UI_FrontEnd_Lab7_ME305.omega()']]],
  ['omega_5fmeas_531',['Omega_meas',['../classClosedLoop__ME305_1_1ClosedLoop.html#a1c9847b61dd73954b0d6f68310793cae',1,'ClosedLoop_ME305::ClosedLoop']]],
  ['omega_5fref_532',['Omega_ref',['../classClosedLoop__ME305_1_1ClosedLoop.html#ac515c9e7871b186f61afb983c633985d',1,'ClosedLoop_ME305.ClosedLoop.Omega_ref()'],['../classTask__Controller__Lab6__ME305_1_1Controller.html#a665b41539c70b43814a3fbdd395de84b',1,'Task_Controller_Lab6_ME305.Controller.Omega_ref()'],['../classTask__Controller__Lab7__ME305_1_1Controller.html#a3b69916215149da90108f853fb751ed4',1,'Task_Controller_Lab7_ME305.Controller.Omega_ref()']]]
];
