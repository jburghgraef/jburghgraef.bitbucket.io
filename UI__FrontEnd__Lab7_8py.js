var UI__FrontEnd__Lab7_8py =
[
    [ "ExtractDataPacket", "UI__FrontEnd__Lab7_8py.html#a3feee35764d28ad666fedc1b0194b9ec", null ],
    [ "sendKp", "UI__FrontEnd__Lab7_8py.html#a4bdbff26bbb9e1c63b538d9b958e4a86", null ],
    [ "kp", "UI__FrontEnd__Lab7_8py.html#aa6a56c89d7a893c6cea52a3eafb25144", null ],
    [ "line", "UI__FrontEnd__Lab7_8py.html#a1d52c3733eee613436875a800b054792", null ],
    [ "omega", "UI__FrontEnd__Lab7_8py.html#a0ec50e7024dc9131c1446c1746e2ec05", null ],
    [ "p", "UI__FrontEnd__Lab7_8py.html#ac62449a7e2570af7c1ae5ee4985914ec", null ],
    [ "pos", "UI__FrontEnd__Lab7_8py.html#a5131584e631326b366622ca66b76b785", null ],
    [ "position", "UI__FrontEnd__Lab7_8py.html#a7ae68407ecae6ec0f002875615cb9918", null ],
    [ "ref", "UI__FrontEnd__Lab7_8py.html#a9d82a9c7800a3b46ef303126366d96b3", null ],
    [ "ser", "UI__FrontEnd__Lab7_8py.html#afd10e954ccdfb9e915d402ba9815fe5a", null ],
    [ "size", "UI__FrontEnd__Lab7_8py.html#a9572f05216c9809948a9ee4f897a8d7a", null ],
    [ "t", "UI__FrontEnd__Lab7_8py.html#a6cbb06ab87a67be230726eef24b82e04", null ],
    [ "theta", "UI__FrontEnd__Lab7_8py.html#a23b93c666b3200905ddb7c9b07133823", null ],
    [ "tim", "UI__FrontEnd__Lab7_8py.html#a7d525b4ff07f52432e919e355b76b56a", null ],
    [ "time", "UI__FrontEnd__Lab7_8py.html#a70692d51e5e9d7baa4a302d991de8329", null ],
    [ "timestamp", "UI__FrontEnd__Lab7_8py.html#a77815d8b8a0ff8ededa2944c6db3714b", null ],
    [ "v", "UI__FrontEnd__Lab7_8py.html#ab86ca39b2d63886709528a33bc7d7f79", null ],
    [ "vel", "UI__FrontEnd__Lab7_8py.html#a99de1fb832b43f8b841ec000711617e0", null ],
    [ "velocity", "UI__FrontEnd__Lab7_8py.html#a947903b93833faa8c0a17f0945957cb2", null ],
    [ "x", "UI__FrontEnd__Lab7_8py.html#ac999199b86bf9adf9b9547c8cda49897", null ]
];