var DRV8847_8py =
[
    [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
    [ "DRV8847_channel", "classDRV8847_1_1DRV8847__channel.html", "classDRV8847_1_1DRV8847__channel" ],
    [ "curr", "DRV8847_8py.html#a0137738c90e55938d844d4f731090413", null ],
    [ "DRV8847", "DRV8847_8py.html#ad76f1980fa9d79e9d7ee8da601447623", null ],
    [ "IN1_pin", "DRV8847_8py.html#a71d618a77ecd412ce90b48e554428568", null ],
    [ "IN2_pin", "DRV8847_8py.html#a2d9b9a50b4cb47659d17d7689a88f2f9", null ],
    [ "IN3_pin", "DRV8847_8py.html#a6664d3ad4e7049765630907849c7ce6a", null ],
    [ "IN4_pin", "DRV8847_8py.html#ace808a7ff861cf3e1b87329a8d476549", null ],
    [ "Mot1", "DRV8847_8py.html#a6f237c1e75ebc4271edd56ccf79c799c", null ],
    [ "Mot2", "DRV8847_8py.html#ab1e00caf942aaa0954198b9e1b40fce6", null ],
    [ "nFAULT_pin", "DRV8847_8py.html#a953375ce23a57d800a97109ebca01096", null ],
    [ "nSLEEP_pin", "DRV8847_8py.html#a0b26b287e9ed9d2bf0d8bfb3e0c62431", null ],
    [ "start", "DRV8847_8py.html#a4feab65d01a6b9e31358be22d59a0962", null ],
    [ "tim", "DRV8847_8py.html#ae25975f558deaa5c891e5f52a4c59b54", null ]
];