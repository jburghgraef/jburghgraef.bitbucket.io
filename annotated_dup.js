var annotated_dup =
[
    [ "BLEModDriver_ME305", null, [
      [ "BLEDriver", "classBLEModDriver__ME305_1_1BLEDriver.html", "classBLEModDriver__ME305_1_1BLEDriver" ]
    ] ],
    [ "ClosedLoop_ME305", null, [
      [ "ClosedLoop", "classClosedLoop__ME305_1_1ClosedLoop.html", "classClosedLoop__ME305_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask", null, [
      [ "ControllerTask", "classControllerTask_1_1ControllerTask.html", "classControllerTask_1_1ControllerTask" ]
    ] ],
    [ "DataCollection", null, [
      [ "DataCollection", "classDataCollection_1_1DataCollection.html", "classDataCollection_1_1DataCollection" ]
    ] ],
    [ "DataCollectionTask_ME305", null, [
      [ "DCTask", "classDataCollectionTask__ME305_1_1DCTask.html", "classDataCollectionTask__ME305_1_1DCTask" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847_channel", "classDRV8847_1_1DRV8847__channel.html", "classDRV8847_1_1DRV8847__channel" ]
    ] ],
    [ "Elevator_FSM_ME305", null, [
      [ "Button", "classElevator__FSM__ME305_1_1Button.html", "classElevator__FSM__ME305_1_1Button" ],
      [ "MotorDriver", "classElevator__FSM__ME305_1_1MotorDriver.html", "classElevator__FSM__ME305_1_1MotorDriver" ],
      [ "TaskElevator", "classElevator__FSM__ME305_1_1TaskElevator.html", "classElevator__FSM__ME305_1_1TaskElevator" ]
    ] ],
    [ "Encoder_Driver_Lab6_ME305", null, [
      [ "EncoderDriver", "classEncoder__Driver__Lab6__ME305_1_1EncoderDriver.html", "classEncoder__Driver__Lab6__ME305_1_1EncoderDriver" ]
    ] ],
    [ "Encoder_Driver_ME305", null, [
      [ "encoderdriver", "classEncoder__Driver__ME305_1_1encoderdriver.html", "classEncoder__Driver__ME305_1_1encoderdriver" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "EncoderTask", null, [
      [ "EncoderTask", "classEncoderTask_1_1EncoderTask.html", "classEncoderTask_1_1EncoderTask" ]
    ] ],
    [ "Lab5UITask_ME305", null, [
      [ "UITaskL5", "classLab5UITask__ME305_1_1UITaskL5.html", "classLab5UITask__ME305_1_1UITaskL5" ]
    ] ],
    [ "LED_FSM_ME305", null, [
      [ "LED", "classLED__FSM__ME305_1_1LED.html", "classLED__FSM__ME305_1_1LED" ],
      [ "TaskRealLED", "classLED__FSM__ME305_1_1TaskRealLED.html", "classLED__FSM__ME305_1_1TaskRealLED" ],
      [ "TaskVirtualLED", "classLED__FSM__ME305_1_1TaskVirtualLED.html", "classLED__FSM__ME305_1_1TaskVirtualLED" ]
    ] ],
    [ "mcp9808", null, [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "Motor_Driver_ME305", null, [
      [ "MotorDriver", "classMotor__Driver__ME305_1_1MotorDriver.html", "classMotor__Driver__ME305_1_1MotorDriver" ]
    ] ],
    [ "MotorTask", null, [
      [ "MotorTask", "classMotorTask_1_1MotorTask.html", "classMotorTask_1_1MotorTask" ]
    ] ],
    [ "Task_Controller_Lab6_ME305", null, [
      [ "Controller", "classTask__Controller__Lab6__ME305_1_1Controller.html", "classTask__Controller__Lab6__ME305_1_1Controller" ]
    ] ],
    [ "Task_Controller_Lab7_ME305", null, [
      [ "Controller", "classTask__Controller__Lab7__ME305_1_1Controller.html", "classTask__Controller__Lab7__ME305_1_1Controller" ]
    ] ],
    [ "Task_User_Lab6_ME305", null, [
      [ "TaskUser", "classTask__User__Lab6__ME305_1_1TaskUser.html", "classTask__User__Lab6__ME305_1_1TaskUser" ]
    ] ],
    [ "Task_User_Lab7_ME305", null, [
      [ "TaskUser", "classTask__User__Lab7__ME305_1_1TaskUser.html", "classTask__User__Lab7__ME305_1_1TaskUser" ]
    ] ],
    [ "TaskEncoder_ME305", null, [
      [ "TaskEncoder", "classTaskEncoder__ME305_1_1TaskEncoder.html", "classTaskEncoder__ME305_1_1TaskEncoder" ]
    ] ],
    [ "TaskUserInterface_ME305", null, [
      [ "TaskUI", "classTaskUserInterface__ME305_1_1TaskUI.html", "classTaskUserInterface__ME305_1_1TaskUI" ]
    ] ],
    [ "TouchPanelDriver", null, [
      [ "TouchPanelDriver", "classTouchPanelDriver_1_1TouchPanelDriver.html", "classTouchPanelDriver_1_1TouchPanelDriver" ]
    ] ],
    [ "TPTask", null, [
      [ "TPTask", "classTPTask_1_1TPTask.html", "classTPTask_1_1TPTask" ]
    ] ],
    [ "UserTask", null, [
      [ "UserTask", "classUserTask_1_1UserTask.html", "classUserTask_1_1UserTask" ]
    ] ]
];