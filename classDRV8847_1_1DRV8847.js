var classDRV8847_1_1DRV8847 =
[
    [ "__init__", "classDRV8847_1_1DRV8847.html#a986dbc8630e6299806253829292cb4ff", null ],
    [ "channel", "classDRV8847_1_1DRV8847.html#af02e03466f75d52b2f3ce70dc4e2a808", null ],
    [ "clear_fault", "classDRV8847_1_1DRV8847.html#ab9d8538134c831ce274f4ce0feb12abb", null ],
    [ "disable", "classDRV8847_1_1DRV8847.html#acd9dbef9212b3014eab18a57a6e0f13a", null ],
    [ "enable", "classDRV8847_1_1DRV8847.html#a57adaca9549fa5935979ff8f0bcdda13", null ],
    [ "fault_CB", "classDRV8847_1_1DRV8847.html#a016b37a93084a4a044ab6b8a031cdcb4", null ],
    [ "fault", "classDRV8847_1_1DRV8847.html#a963c4934488e87932aabe3c6758f4596", null ],
    [ "lastFault", "classDRV8847_1_1DRV8847.html#af07ef25735d287c441e4cb5924e26761", null ],
    [ "nFAULT", "classDRV8847_1_1DRV8847.html#ac1cf4b2c8547051b9f4aaf0a5e5f77c6", null ],
    [ "nSLEEP", "classDRV8847_1_1DRV8847.html#aedae22eb42efbed4ef2608c52ff1e43a", null ]
];