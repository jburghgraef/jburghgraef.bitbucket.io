var files_dup =
[
    [ "BLEModDriver_ME305.py", "BLEModDriver__ME305_8py.html", [
      [ "BLEDriver", "classBLEModDriver__ME305_1_1BLEDriver.html", "classBLEModDriver__ME305_1_1BLEDriver" ]
    ] ],
    [ "ClosedLoop_ME305.py", "ClosedLoop__ME305_8py.html", [
      [ "ClosedLoop", "classClosedLoop__ME305_1_1ClosedLoop.html", "classClosedLoop__ME305_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", [
      [ "ControllerTask", "classControllerTask_1_1ControllerTask.html", "classControllerTask_1_1ControllerTask" ]
    ] ],
    [ "DataCollection.py", "DataCollection_8py.html", [
      [ "DataCollection", "classDataCollection_1_1DataCollection.html", "classDataCollection_1_1DataCollection" ]
    ] ],
    [ "DataCollectionTask_ME305.py", "DataCollectionTask__ME305_8py.html", [
      [ "DCTask", "classDataCollectionTask__ME305_1_1DCTask.html", "classDataCollectionTask__ME305_1_1DCTask" ]
    ] ],
    [ "DCMotorsFD.py", "DCMotorsFD_8py.html", null ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "Elevator_FSM_ME305.py", "Elevator__FSM__ME305_8py.html", [
      [ "TaskElevator", "classElevator__FSM__ME305_1_1TaskElevator.html", "classElevator__FSM__ME305_1_1TaskElevator" ],
      [ "Button", "classElevator__FSM__ME305_1_1Button.html", "classElevator__FSM__ME305_1_1Button" ],
      [ "MotorDriver", "classElevator__FSM__ME305_1_1MotorDriver.html", "classElevator__FSM__ME305_1_1MotorDriver" ]
    ] ],
    [ "Encoder_Driver_Lab6_ME305.py", "Encoder__Driver__Lab6__ME305_8py.html", [
      [ "EncoderDriver", "classEncoder__Driver__Lab6__ME305_1_1EncoderDriver.html", "classEncoder__Driver__Lab6__ME305_1_1EncoderDriver" ]
    ] ],
    [ "Encoder_Driver_ME305.py", "Encoder__Driver__ME305_8py.html", [
      [ "encoderdriver", "classEncoder__Driver__ME305_1_1encoderdriver.html", "classEncoder__Driver__ME305_1_1encoderdriver" ]
    ] ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "EncoderTask.py", "EncoderTask_8py.html", [
      [ "EncoderTask", "classEncoderTask_1_1EncoderTask.html", "classEncoderTask_1_1EncoderTask" ]
    ] ],
    [ "EngReq.py", "EngReq_8py.html", null ],
    [ "EOMDerivation.py", "EOMDerivation_8py.html", null ],
    [ "fibb_gen.py", "fibb__gen_8py.html", "fibb__gen_8py" ],
    [ "Frontend.py", "Frontend_8py.html", "Frontend_8py" ],
    [ "HW0x02.py", "HW0x02_8py.html", null ],
    [ "HW0x04.py", "HW0x04_8py.html", null ],
    [ "HW0x05.py", "HW0x05_8py.html", null ],
    [ "lab03main.py", "lab03main_8py.html", "lab03main_8py" ],
    [ "Lab0x02.py", "Lab0x02_8py.html", null ],
    [ "Lab0x02_Part_A.py", "Lab0x02__Part__A_8py.html", "Lab0x02__Part__A_8py" ],
    [ "Lab0x02_Part_B.py", "Lab0x02__Part__B_8py.html", "Lab0x02__Part__B_8py" ],
    [ "Lab0x03.py", "Lab0x03_8py.html", null ],
    [ "Lab0x04.py", "Lab0x04_8py.html", null ],
    [ "Lab3main.py", "Lab3main_8py.html", "Lab3main_8py" ],
    [ "Lab4main.py", "Lab4main_8py.html", "Lab4main_8py" ],
    [ "lab5main_ME305.py", "lab5main__ME305_8py.html", "lab5main__ME305_8py" ],
    [ "Lab5UITask_ME305.py", "Lab5UITask__ME305_8py.html", [
      [ "UITaskL5", "classLab5UITask__ME305_1_1UITaskL5.html", "classLab5UITask__ME305_1_1UITaskL5" ]
    ] ],
    [ "Lab6shares_ME305.py", "Lab6shares__ME305_8py.html", "Lab6shares__ME305_8py" ],
    [ "Lab7shares_ME305.py", "Lab7shares__ME305_8py.html", "Lab7shares__ME305_8py" ],
    [ "LED_FSM_ME305.py", "LED__FSM__ME305_8py.html", [
      [ "TaskVirtualLED", "classLED__FSM__ME305_1_1TaskVirtualLED.html", "classLED__FSM__ME305_1_1TaskVirtualLED" ],
      [ "TaskRealLED", "classLED__FSM__ME305_1_1TaskRealLED.html", "classLED__FSM__ME305_1_1TaskRealLED" ],
      [ "LED", "classLED__FSM__ME305_1_1LED.html", "classLED__FSM__ME305_1_1LED" ]
    ] ],
    [ "mainElevator_FSM_ME305.py", "mainElevator__FSM__ME305_8py.html", "mainElevator__FSM__ME305_8py" ],
    [ "mainLED_FSM_ME305.py", "mainLED__FSM__ME305_8py.html", "mainLED__FSM__ME305_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "ME305.py", "ME305_8py.html", null ],
    [ "ME405.py", "ME405_8py.html", null ],
    [ "Motor_Driver_ME305.py", "Motor__Driver__ME305_8py.html", "Motor__Driver__ME305_8py" ],
    [ "MotorTask.py", "MotorTask_8py.html", [
      [ "MotorTask", "classMotorTask_1_1MotorTask.html", "classMotorTask_1_1MotorTask" ]
    ] ],
    [ "Results.py", "Results_8py.html", null ],
    [ "SetupDesign.py", "SetupDesign_8py.html", null ],
    [ "Task_Controller_Lab6_ME305.py", "Task__Controller__Lab6__ME305_8py.html", [
      [ "Controller", "classTask__Controller__Lab6__ME305_1_1Controller.html", "classTask__Controller__Lab6__ME305_1_1Controller" ]
    ] ],
    [ "Task_Controller_Lab7_ME305.py", "Task__Controller__Lab7__ME305_8py.html", [
      [ "Controller", "classTask__Controller__Lab7__ME305_1_1Controller.html", "classTask__Controller__Lab7__ME305_1_1Controller" ]
    ] ],
    [ "Task_User_Lab6_ME305.py", "Task__User__Lab6__ME305_8py.html", [
      [ "TaskUser", "classTask__User__Lab6__ME305_1_1TaskUser.html", "classTask__User__Lab6__ME305_1_1TaskUser" ]
    ] ],
    [ "Task_User_Lab7_ME305.py", "Task__User__Lab7__ME305_8py.html", [
      [ "TaskUser", "classTask__User__Lab7__ME305_1_1TaskUser.html", "classTask__User__Lab7__ME305_1_1TaskUser" ]
    ] ],
    [ "TaskEncoder_ME305.py", "TaskEncoder__ME305_8py.html", [
      [ "TaskEncoder", "classTaskEncoder__ME305_1_1TaskEncoder.html", "classTaskEncoder__ME305_1_1TaskEncoder" ]
    ] ],
    [ "TasksOverview.py", "TasksOverview_8py.html", null ],
    [ "TaskUserInterface_ME305.py", "TaskUserInterface__ME305_8py.html", [
      [ "TaskUI", "classTaskUserInterface__ME305_1_1TaskUI.html", "classTaskUserInterface__ME305_1_1TaskUI" ]
    ] ],
    [ "Term_Project.py", "Term__Project_8py.html", null ],
    [ "TouchPanelDriver.py", "TouchPanelDriver_8py.html", "TouchPanelDriver_8py" ],
    [ "TouchPanelTest.py", "TouchPanelTest_8py.html", "TouchPanelTest_8py" ],
    [ "TPCalibration.py", "TPCalibration_8py.html", null ],
    [ "TPShares.py", "TPShares_8py.html", "TPShares_8py" ],
    [ "TPTask.py", "TPTask_8py.html", [
      [ "TPTask", "classTPTask_1_1TPTask.html", "classTPTask_1_1TPTask" ]
    ] ],
    [ "UI_FrontEnd_Lab6_ME305.py", "UI__FrontEnd__Lab6__ME305_8py.html", "UI__FrontEnd__Lab6__ME305_8py" ],
    [ "UI_FrontEnd_Lab7_ME305.py", "UI__FrontEnd__Lab7__ME305_8py.html", "UI__FrontEnd__Lab7__ME305_8py" ],
    [ "UI_FrontEnd_ME305.py", "UI__FrontEnd__ME305_8py.html", "UI__FrontEnd__ME305_8py" ],
    [ "UserTask.py", "UserTask_8py.html", [
      [ "UserTask", "classUserTask_1_1UserTask.html", "classUserTask_1_1UserTask" ]
    ] ],
    [ "Vendotron.py", "Vendotron_8py.html", "Vendotron_8py" ]
];