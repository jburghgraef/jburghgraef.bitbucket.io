/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mechatronics Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "ME 305: Intro to Mechatronics", "index.html#sec_ME305", null ],
    [ "ME 405: Mechatronics", "index.html#sec_ME405", null ],
    [ "ME 405 Term Project", "index.html#sec_TP", null ],
    [ "ME305", "ME305.html", [
      [ "ME 305: Intro to Mechatronics Assignments", "ME305.html#sec_titleME305", null ],
      [ "Homework 00", "ME305.html#sec_HW00", null ],
      [ "Fibbonacci Sequence (Lab 1)", "ME305.html#sec_FibSeq", null ],
      [ "LED Pattern (Lab 2)", "ME305.html#sec_LEDPatt", null ],
      [ "Encoder Driver (Lab 3)", "ME305.html#sec_EncDriver", null ],
      [ "Extending Your Interface (Lab 4)", "ME305.html#sec_ExtendInterface", null ],
      [ "Use Your Interface (Lab 5)", "ME305.html#sec_UseInterface", null ],
      [ "DC Motors (Lab 6)", "ME305.html#sec_DCMotors", null ],
      [ "Reference Tracking (Lab 7)", "ME305.html#sec_RefTracking", null ]
    ] ],
    [ "ME405", "ME405.html", [
      [ "ME 405: Mechatronics Assignments", "ME405.html#sec_titleME405", null ],
      [ "Virtual Vending Machine (Lab 1)", "ME405.html#sec_ven", null ],
      [ "Think Fast! (Lab 2)", "ME405.html#sec_lab2ME405", null ],
      [ "Pushing the Right Buttons (Lab 3)", "ME405.html#sec_lab3ME405", null ],
      [ "Hot or Not? (Lab 4)", "ME405.html#sec_lab4ME405", null ]
    ] ],
    [ "ME405TermProject", "ME405TermProject.html", [
      [ "ME 405 Term Project Landing Page", "ME405TermProject.html#sec_titleTPLP", null ],
      [ "Summary", "ME405TermProject.html#sec_summ", null ],
      [ "System Calculations", "ME405TermProject.html#sec_SC", null ],
      [ "Project Setup and Design", "ME405TermProject.html#sec_setup", null ],
      [ "Engineering Requirements", "ME405TermProject.html#sec_EngReq", null ],
      [ "Calibration and Tuning", "ME405TermProject.html#sec_Calib", null ],
      [ "Overview of Tasks", "ME405TermProject.html#sec_taskOv", null ],
      [ "Obstacles", "ME405TermProject.html#sec_obs", null ],
      [ "Results and Discussion", "ME405TermProject.html#sec_TPresults", null ],
      [ "Source Code", "ME405TermProject.html#sec_src", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BLEModDriver__ME305_8py.html",
"UI__FrontEnd__Lab7__ME305_8py.html#a1ee6c4e6a1bc8208ea3e5e4e0e83a10c",
"classLab5UITask__ME305_1_1UITaskL5.html#afbcb66709620fed53401b6fedbdd81ce",
"index.html#sec_TP"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';