var UI__FrontEnd__Lab7__ME305_8py =
[
    [ "ExtractDataPacket", "UI__FrontEnd__Lab7__ME305_8py.html#a88b81884f108cbc46a8cebb8fba6bd50", null ],
    [ "sendKp", "UI__FrontEnd__Lab7__ME305_8py.html#ac037e23f5a2f60f1b5d0307817ce2e2c", null ],
    [ "kp", "UI__FrontEnd__Lab7__ME305_8py.html#a035d0964653a577a867c2e719e5628f4", null ],
    [ "line", "UI__FrontEnd__Lab7__ME305_8py.html#a101d63dc788da4e3317c1f8cb27f461f", null ],
    [ "omega", "UI__FrontEnd__Lab7__ME305_8py.html#a8e6d9571432c9d4a27613e8856f33870", null ],
    [ "p", "UI__FrontEnd__Lab7__ME305_8py.html#af43b433e7efab51022bba86d1087647c", null ],
    [ "pos", "UI__FrontEnd__Lab7__ME305_8py.html#ad5019d2150ce3a86a8dd3caea70702d5", null ],
    [ "position", "UI__FrontEnd__Lab7__ME305_8py.html#add70e600c883bc2059dee8c900f36dfd", null ],
    [ "ref", "UI__FrontEnd__Lab7__ME305_8py.html#a2abd670ca7037fa3cd39a19e064cea51", null ],
    [ "ser", "UI__FrontEnd__Lab7__ME305_8py.html#aa027c2aac999f2b409c01290ed52a2bc", null ],
    [ "size", "UI__FrontEnd__Lab7__ME305_8py.html#a50074a2ca53c1120186c35137f865539", null ],
    [ "t", "UI__FrontEnd__Lab7__ME305_8py.html#aa7249abb15f311f1c86e370707633539", null ],
    [ "theta", "UI__FrontEnd__Lab7__ME305_8py.html#af097ee130a27e138bade36880c8f84e2", null ],
    [ "tim", "UI__FrontEnd__Lab7__ME305_8py.html#af31f00ec99be71a4b523e9e6e021f815", null ],
    [ "time", "UI__FrontEnd__Lab7__ME305_8py.html#aef5536d23161f9a63a83c79cc72cbd8d", null ],
    [ "timestamp", "UI__FrontEnd__Lab7__ME305_8py.html#acf1ba0420c85eb14f0a960408bc05e8e", null ],
    [ "v", "UI__FrontEnd__Lab7__ME305_8py.html#a8c7d0268bd820b30c6d35ed9baad1178", null ],
    [ "vel", "UI__FrontEnd__Lab7__ME305_8py.html#aeed96f9c9b2ff79d5996024ee1b24052", null ],
    [ "velocity", "UI__FrontEnd__Lab7__ME305_8py.html#a64858fdd5089a1e272fe80bd98405f0c", null ],
    [ "x", "UI__FrontEnd__Lab7__ME305_8py.html#a1ee6c4e6a1bc8208ea3e5e4e0e83a10c", null ]
];