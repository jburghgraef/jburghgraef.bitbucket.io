var Lab0x02__Part__B_8py =
[
    [ "genWaitTime", "Lab0x02__Part__B_8py.html#a64de8724328291f0087995f0bd45dd73", null ],
    [ "IC_cb", "Lab0x02__Part__B_8py.html#aca3452d4f2402d578cb576e7fb53e970", null ],
    [ "OC_cb", "Lab0x02__Part__B_8py.html#aada386a10705e6573a69aaf6881d67ea", null ],
    [ "avg_rxn", "Lab0x02__Part__B_8py.html#a6bfaaab7f264caef08df8486e7e16ca4", null ],
    [ "button_press", "Lab0x02__Part__B_8py.html#a7cf24f4274a299ebbf6ec956aeccc24c", null ],
    [ "IC_value", "Lab0x02__Part__B_8py.html#a4a96959c9a4c549977ddcf662ba84940", null ],
    [ "last_compare", "Lab0x02__Part__B_8py.html#ad5733022dd6ddb2290513a5ed36f5559", null ],
    [ "LED", "Lab0x02__Part__B_8py.html#a4b5e79a8b1bd85b88a0cb7706593f1b3", null ],
    [ "LED_dur", "Lab0x02__Part__B_8py.html#af077a57c2a9e57be4575a7798369fecc", null ],
    [ "n", "Lab0x02__Part__B_8py.html#ae2fa1bed3762e3c6e0c26786389f74f5", null ],
    [ "pinA5", "Lab0x02__Part__B_8py.html#ad5c7bade0843416e66b4c58942104669", null ],
    [ "PinA5", "Lab0x02__Part__B_8py.html#a5cfda9794c04fcdaf06b24670b2c47e2", null ],
    [ "pinB3", "Lab0x02__Part__B_8py.html#af37727a04890ea28a588ecbe9d5cab7e", null ],
    [ "PS", "Lab0x02__Part__B_8py.html#aee87761621402a73ae013ef3802bcae3", null ],
    [ "rxn_sum", "Lab0x02__Part__B_8py.html#aedfbd7b239789a650a8744bdc698e1ea", null ],
    [ "rxn_time", "Lab0x02__Part__B_8py.html#aa98ebdddf714015093674b4b593cb442", null ],
    [ "rxn_times", "Lab0x02__Part__B_8py.html#a8b34e64030f70033f75a098f8729ac1c", null ],
    [ "T2CH1", "Lab0x02__Part__B_8py.html#a963489ce2c673f55b4f64e6cf727d4c4", null ],
    [ "T2CH2", "Lab0x02__Part__B_8py.html#a56c95dc3005a6fb58be757fce8e49a51", null ],
    [ "Tim2", "Lab0x02__Part__B_8py.html#a86a00d4715f89d94ddc817d882d327b3", null ]
];