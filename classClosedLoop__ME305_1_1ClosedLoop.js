var classClosedLoop__ME305_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop__ME305_1_1ClosedLoop.html#a6a78db2cf7a0be82601282f397758f54", null ],
    [ "getKp", "classClosedLoop__ME305_1_1ClosedLoop.html#a0c920ded1b4cf8f7b7b9bd69d666bd79", null ],
    [ "setKp", "classClosedLoop__ME305_1_1ClosedLoop.html#a0a32c70d84b66bc0160d2e88e0cf06ea", null ],
    [ "update", "classClosedLoop__ME305_1_1ClosedLoop.html#a3cc23bcfc4cf49fb5421b4a49516f5be", null ],
    [ "Kp", "classClosedLoop__ME305_1_1ClosedLoop.html#a039116bc5fee6d376ad329e911565053", null ],
    [ "L", "classClosedLoop__ME305_1_1ClosedLoop.html#a76e0faee64eaa80fdb390dd6743ea1ae", null ],
    [ "Omega_meas", "classClosedLoop__ME305_1_1ClosedLoop.html#a1c9847b61dd73954b0d6f68310793cae", null ],
    [ "Omega_ref", "classClosedLoop__ME305_1_1ClosedLoop.html#ac515c9e7871b186f61afb983c633985d", null ]
];