var classClosedLoop_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop_1_1ClosedLoop.html#a4cea6bab4bd9d8aa4dec477adce9f984", null ],
    [ "getKp", "classClosedLoop_1_1ClosedLoop.html#abc8fff7f0286415992094b299bd726ff", null ],
    [ "setKp", "classClosedLoop_1_1ClosedLoop.html#aba51ee735a8c2a878e43ac95fd6b2bad", null ],
    [ "update", "classClosedLoop_1_1ClosedLoop.html#a5c078dd85ab611ca7191ddcaab9c1089", null ],
    [ "Kp", "classClosedLoop_1_1ClosedLoop.html#a600c2ee167190f2afdf9fc38c849cf01", null ],
    [ "L", "classClosedLoop_1_1ClosedLoop.html#add9a36f67edcf30547d56b1676d53b92", null ],
    [ "Omega_meas", "classClosedLoop_1_1ClosedLoop.html#aa0f2a0dcfdc2726f15edec793a62f613", null ],
    [ "Omega_ref", "classClosedLoop_1_1ClosedLoop.html#a096f07e9814829b916e6f9f5d12e11cf", null ]
];